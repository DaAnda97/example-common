pipeline {

    agent any

    environment {
        dockerImageTag = sh(returnStdout: true, script: '[ "$GIT_BRANCH" = "master" ] && echo latest || echo $GIT_BRANCH')

        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "https"
        NEXUS_URL = "nexus.andreasriepl.de"
        NEXUS_REPOSITORY = "example"
        NEXUS_CREDENTIAL_ID = "nexus"
    }

  stages {

    stage('Setup') {
      when { expression { env.BRANCH_NAME ==~ /master/  } }
      steps {
              withCredentials([usernamePassword(credentialsId: 'nexus', passwordVariable: 'pass', usernameVariable: 'user')]) {
                  sh "./gradlew -P nexusUser=$user -P nexusPassword=$pass clean"
              }
            }
    }

    stage('Test') {
      when { expression { env.BRANCH_NAME ==~ /master/  } }
      steps {
              withCredentials([usernamePassword(credentialsId: 'nexus', passwordVariable: 'pass', usernameVariable: 'user')]) {
                  sh "./gradlew -P nexusUser=$user -P nexusPassword=$pass test"
              }
            }
    }

    stage('Package') {
      when { expression { env.BRANCH_NAME ==~ /master/  } }
      steps {
              withCredentials([usernamePassword(credentialsId: 'nexus', passwordVariable: 'pass', usernameVariable: 'user')]) {
                  sh "./gradlew -P nexusUser=$user -P nexusPassword=$pass jar"
              }
            }
    }

    stage('Create Pom') {
      when { expression { env.BRANCH_NAME ==~ /master/  } }
      steps {
              withCredentials([usernamePassword(credentialsId: 'nexus', passwordVariable: 'pass', usernameVariable: 'user')]) {
                  sh "./gradlew -P nexusUser=$user -P nexusPassword=$pass createPom"
              }
            }
    }

    stage("Publish to Nexus") {
        steps {
            script {
                // Read POM xml file using 'readMavenPom' step , this step 'readMavenPom' is included in: https://plugins.jenkins.io/pipeline-utility-steps
                pom = readMavenPom file: "pom.xml";
                version = pom.version
                group = pom.groupId
                artifactId = pom.artifactId

                // Extract the path from the File found
                artifactPath = "./build/libs/example-common-${version}.jar"

                // Assign to a boolean response verifying If the artifact name exists
                artifactExists = fileExists artifactPath;
                if(artifactExists) {
                    nexusArtifactUploader(
                        nexusVersion: NEXUS_VERSION,
                        protocol: NEXUS_PROTOCOL,
                	    nexusUrl: NEXUS_URL,
                        groupId: group,
                        version: version,
                        repository: NEXUS_REPOSITORY,
                        credentialsId: NEXUS_CREDENTIAL_ID,
                        artifacts: [
                            // Artifact generated such as .jar, .ear and .war files.
                            [artifactId: artifactId,
                            classifier: '',
                            file: artifactPath,
                            type: "jar"],
                            // Lets upload the pom.xml file for additional information for Transitive dependencies
                            [artifactId: artifactId,
                            classifier: '',
                            file: "pom.xml",
                            type: "pom"]
                        ]
                    );
                } else {
                    error "*** File: ${artifactPath}, could not be found";
                }
            }
        }
    }

    stage ('Telling Childs') {
            steps {
               build job: 'example-service/master',wait: false
            }
    }

  }
}