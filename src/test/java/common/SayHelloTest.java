package common;

import org.junit.Test;
import static org.junit.Assert.*;

public class SayHelloTest {

    @Test
    public void testConcatenate() {
        SayHello sayHello = new SayHello();

        String result = sayHello.getContent();

        assertEquals("Hello", result);
    }
}
